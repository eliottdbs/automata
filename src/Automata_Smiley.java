/**
 * @author DUBOIS Eliott
 * @version 1.0
 * Created  on : 2019-12-05
 * Modified on : 2019-12-05
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains all the necessary to check a smiley.
 */
public class Automata_Smiley {

    /**
     * Build the automata to test a string as a smiley.
     * This automata accept only the following smileys :
     *      :-)     :-(     ;-)     ;-(     :=)     :=(
     * @return the built automata
     */
    static Automata SmileyCharger() {

        Automata a = new Automata();

        // Set Alphabet
        List<String> alphabet = new LinkedList<>(Arrays.asList(":", ";", "-", "=", ")", "("));
        a.setAlphabet(alphabet);

        // Set States
        List<State> states = new LinkedList<>();
        State state0 = new State("State0");
        State eye1 = new State("Eye1");
        State eye2 = new State("Eye2");
        State nose = new State("Nose");
        State mouth = new State("Mouth");
        state0.addTransition(":", eye1);
        state0.addTransition(";", eye2);
        eye1.addTransition("-", nose);
        eye1.addTransition("=", nose);
        eye2.addTransition("-", nose);
        nose.addTransition("(", mouth);
        nose.addTransition(")", mouth);
        states.add(state0);
        states.add(eye1);
        states.add(eye2);
        states.add(nose);
        states.add(mouth);
        a.setStates(states);

        // Set initial state
        a.setInitialState(state0);

        // Set finals States
        List<State> fStates = new LinkedList<>();
        fStates.add(mouth);
        a.setFinalsStates(fStates);

        return a;
    }

    /**
     * Method to test an automata of smileys with given strings.
     * Use the method SmileyCharger().
     */
    static void SmileyTester() {
        Automata a = SmileyCharger();
        String strings[] = {":-)", ":-(", ";-)", ";-(", ":=)", ":=(", ";=)", ":p", ":-D", "O:-)"};

        for (String str: strings) {
            Application.execOne(str, a);
        }
    }
}
