/**
 * @author DUBOIS Eliott
 * @version 1.0
 * Created  on : 2019-11-12
 * Modified on : 2019-12-05
 */

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains the attributes and methods that represent an automata.
 * Automatas are used to check the correct order of something.
 */
public class Automata {
    private List<State> states;
    private HashMap<String, State> delta;
    private State initialState;
    private List<State> finalsStates;
    private List<String> alphabet;

    /**
     * Initialize an automata.
     */
    public Automata() {
        this.states = new LinkedList<>();
        this.delta = new HashMap<>();
        this.finalsStates = new LinkedList<>();
        this.alphabet = new LinkedList<>();
    }

    /**
     * Getter of the automata's list of states.
     * @return the list of states
     */
    public List<State> getStates() {
        return states;
    }

    /**
     * Setter of the automata's states list.
     * @param states the new states list
     */
    public void setStates(List<State> states) {
        this.states = states;
    }

    /**
     * Getter of the automata's delta.
     * delta is the list of all the transitions of all the states of the automata.
     * @return HashMap<Label, State>
     */
    public HashMap<String, State> getDelta() {
        return delta;
    }

    /**
     * Getter of the automata's initial state.
     * @return the initial state
     */
    public State getInitialState() {
        return initialState;
    }

    /**
     * Setter of the automata's initial state.
     * @param initialState the new initial state
     */
    public void setInitialState(State initialState) {
        this.initialState = initialState;
    }

    /**
     * Getter of the automata's list of finals states.
     * @return the list of finals states
     */
    public List<State> getFinalsStates() {
        return finalsStates;
    }

    /**
     * Setter of the automata's finals states.
     * @param finalsStates the new list of finals states
     */
    public void setFinalsStates(List<State> finalsStates) {
        this.finalsStates = finalsStates;
    }

    /**
     * Getter of the automata's alphabet.
     * @return the alphabet
     */
    public List<String> getAlphabet() {
        return alphabet;
    }

    /**
     * Setter of the automata's alphabet.
     * @param alphabet the new alphabet
     */
    public void setAlphabet(List<String> alphabet) {
        this.alphabet = alphabet;
    }

    /**
     * Method to test if an object matches with the automata.
     * With a given string, tests if that string match with the automata.
     * @param string the string containing the object
     * @return a boolean if it matches or not
     */
    public boolean objectTester(String string) {
        int index = 0;
        int strSize = string.length();
        State currentState = this.getInitialState();

        while ((!finalsStates.contains(currentState) || index < strSize) && currentState != null) {

            if (index + 1 <= strSize) {
                String currentChar = string.substring(index, index + 1);
                currentState = currentState.nextState(currentChar);
            } else {
                currentState = null;
            }

            index = index + 1;
        }

        return finalsStates.contains(currentState) && strSize == index;
    }


    /**
     * Method to test if N objects match with the automata.
     * Example with smileys : ":-):-(" returns true and ":-)O:-)" returns false.
     * @param string the string containing N objects
     * @return true or false if the string is correct or not
     */
    public boolean nObjectTester(String string) {
        int index = 0;
        State currentState = this.getInitialState();

        while (!finalsStates.contains(currentState) && currentState != null) {

            if (index + 1 <= string.length()) {
                String currentChar = string.substring(index, index + 1);
                currentState = currentState.nextState(currentChar);
                if (finalsStates.contains(currentState) && string.length() > index + 1) {
                    currentState = this.getInitialState();
                }
            } else {
                currentState = null;
            }

            index = index + 1;
        }
        return finalsStates.contains(currentState);
    }
}