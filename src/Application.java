/**
 * @author DUBOIS Eliott
 * @version 1.0
 * Created  on : 2019-11-19
 * Modified on : 2019-12-05
 */

import java.util.Scanner;

/**
 * This class contains the application to run automatas with the main function.
 */
public class Application {

    /**
     * Method to print the menu of the application.
     */
    static void menu() {
        System.out.println();
        System.out.println("=================== MENU ===================");
        System.out.println("Type the number of your selection to test it");
        System.out.println("    1. Recognize a/multiple time(s)");
        System.out.println("    2. Recognize a/multiple smiley(s)");
        System.out.println("    3. Recognize a/multiple date(s)");
        System.out.println("    4. Recognize an/multiple email(s)");
        System.out.println("    5. Recognize a/multiple polynomial(s)");
        System.out.println("    0. Quit");
        System.out.println("============================================");
        System.out.print("Your selection : ");
    }

    /**
     * Method to test one object with an given automata.
     * @param input the string to test
     * @param automata the given automata
     */
    static void execOne(String input, Automata automata) {
        if (automata.objectTester(input)) {
            System.out.print("[\u001B[32mCORRECT\u001B[39m]  ");
        } else {
            System.out.print("[\u001B[31mINCORRECT\u001B[39m]");
        }
        System.out.println("\t\t"+input);
    }

    /**
     * Method to test multiple objects with an given automata.
     * Pre-condition : Multiple objects are written in a row, without separator.
     * @param input the string to test
     * @param automata the given automata
     */
    static void execN(String input, Automata automata) {
        if (automata.nObjectTester(input)) {
            System.out.print("[\u001B[32mCORRECT\u001B[39m]  ");
        } else {
            System.out.print("[\u001B[31mINCORRECT\u001B[39m]");
        }
        System.out.println("\t\t"+input);
    }

    /**
     * Main function : Test application
     * @param args are not needed
     */
    public static void main(String[] args) {

        // Input init
        Scanner scan = new Scanner(System.in);
        String choice = "1";

        // Main loop
        while (!choice.equals("0")) {
            // Output menu
            menu();

            // Input choice
            choice = scan.nextLine();

            switch (choice) {
                case "1":
                    System.out.println("If you want to execute automatic test, please enter '/auto',");
                    System.out.print("Else, to recognize one or more time, please enter the string that characterizes it/them: ");
                    String input_time = scan.nextLine();

                    // Auto-Test ?
                    if (input_time.equals("/auto")) {
                        Automata_Time.TimeTester();
                    } else {
                        Automata time = Automata_Time.TimeCharger();
                        execN(input_time, time);
                    }

                    break;

                case "2":
                    System.out.println("If you want to execute automatic test, please enter '/auto',");
                    System.out.print("To recognize one or more smiley, please enter the string that characterizes it/them: ");
                    String input_smiley = scan.nextLine();

                    // Auto-Test ?
                    if (input_smiley.equals("/auto")) {
                        Automata_Smiley.SmileyTester();
                    } else {
                        Automata smiley = Automata_Smiley.SmileyCharger();
                        execN(input_smiley, smiley);
                    }

                    break;

                case "3":
                    System.out.println("If you want to execute automatic test, please enter '/auto',");
                    System.out.print("To recognize one or more date, please enter the string that characterizes it/them: ");
                    String input_date = scan.nextLine();

                    // Auto-Test ?
                    if (input_date.equals("/auto")) {
                        Automata_Date.DateTester();
                    } else {
                        Automata date = Automata_Date.DateCharger();
                        execN(input_date, date);
                    }

                    break;

                case "4":
                    System.out.println("If you want to execute automatic test, please enter '/auto',");
                    System.out.println("To recognize one or more email address, please enter the string that characterizes it/them.");
                    System.out.print("(Note : multiple addresses are written with ';' as separator): ");
                    String input_email = scan.nextLine();

                    // Auto-Test ?
                    if (input_email.equals("/auto")) {
                        Automata_Email.EmailTester();
                    } else {
                        Automata email = Automata_Email.EmailCharger();
                        execOne(input_email, email);
                    }

                    break;

                case "5":
                    System.out.println("If you want to execute automatic test, please enter '/auto',");
                    System.out.println("To recognize one or more email address, please enter the string that characterizes it/them:");
                    String input_polynomial = scan.nextLine();

                    // Auto-Test ?
                    if (input_polynomial.equals("/auto")) {
                        Automata_Polynomial.PolynomialTester();
                    } else {
                        Automata polynomial = Automata_Polynomial.PolynomialCharger();
                        execOne(input_polynomial, polynomial);
                    }

                    break;

                // If the choice is anything else that the previous choices, then it quits
                default:
                    choice = "0";
                    break;

            }

        }

    }

}
