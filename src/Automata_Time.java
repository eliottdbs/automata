/**
 * @author DUBOIS Eliott
 * @version 1.0
 * Created  on : 2019-12-05
 * Modified on : 2019-12-05
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains all the necessary to check a time.
 */
public class Automata_Time {

    /**
     * Build the automata to test a string as a time.
     * The accepted format is HH:MM.
     * @return the built automata
     */
    static Automata TimeCharger() {

        Automata a = new Automata();

        // Set Alphabet
        List<String> numbers = new LinkedList<>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));
        List<String> specialCaracters = new LinkedList<>(Arrays.asList(":"));

        List<String> alphabet = new LinkedList<>();
        alphabet.addAll(numbers);
        alphabet.addAll(specialCaracters);
        a.setAlphabet(alphabet);

        // Set States
        List<State> states = new LinkedList<>();
        State state0 = new State("State0");
        State hour1 = new State("Hour1");
        State hour2 = new State("Hour2");
        State hour = new State("Hour");
        State minute1 = new State("Minute1");
        State minute2 = new State("Minute2");
        State minute = new State("Minute");

        state0.addTransition("0", hour1);
        state0.addTransition("1", hour1);
        state0.addTransition("2", hour2);

        for (String str: numbers) {
            hour1.addTransition(str, hour);
        }

        hour2.addTransition("0", hour);
        hour2.addTransition("1", hour);
        hour2.addTransition("2", hour);
        hour2.addTransition("3", hour);

        hour.addTransition(":", minute1);

        minute1.addTransition("0", minute2);
        minute1.addTransition("1", minute2);
        minute1.addTransition("2", minute2);
        minute1.addTransition("3", minute2);
        minute1.addTransition("4", minute2);
        minute1.addTransition("5", minute2);

        for (String str: numbers) {
            minute2.addTransition(str, minute);
        }

        states.add(state0);
        states.add(hour1);
        states.add(hour2);
        states.add(hour);
        states.add(minute1);
        states.add(minute2);
        states.add(minute);
        a.setStates(states);

        // Set initial state
        a.setInitialState(state0);

        // Set finals States
        List<State> fStates = new LinkedList<>();
        fStates.add(minute);
        a.setFinalsStates(fStates);

        return a;
    }

    /**
     * Method to test an automata of times with given strings.
     * Use the method TimeCharger().
     */
    static void TimeTester() {
        Automata a = TimeCharger();
        String strings[] = {"00:00", "23:59", "01:01", "07:52", "09:09", "24:01", "05:60", "31:99", "08:1", "11:1111"};

        for (String str: strings) {
            Application.execOne(str, a);
        }
    }
}
