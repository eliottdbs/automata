/**
 * @author DUBOIS Eliott
 * @version 1.0
 * Created  on : 2019-12-05
 * Modified on : 2019-12-05
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains all the necessary to check an email address.
 */
public class Automata_Email {

    /**
     * Build the automata to test a string as an email.
     * This automata does not accept all email addresses's occurencies, here some examples :
     * - aZ@aZ.aZ
     * - aZ09-aZ09@aZ09.aZ
     * - aZ09-aZ09.aZ09-aZ09@aZ09-aZ09.aZ
     * @return the built automata
     */
    static Automata EmailCharger() {

        Automata a = new Automata();

        // Set Alphabet
        List<String> letters = new LinkedList<>(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"));
        List<String> numbers = new LinkedList<>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));
        List<String> specialCaracters = new LinkedList<>(Arrays.asList(";", ".", "-", "@"));

        List<String> alphabet = new LinkedList<>();
        alphabet.addAll(letters);
        alphabet.addAll(numbers);
        alphabet.addAll(specialCaracters);
        a.setAlphabet(alphabet);

        // Set States
        List<State> states = new LinkedList<>();
        State e0 = new State("E0");
        State e1 = new State("E1");
        State e2 = new State("E2");
        State e3 = new State("E3");
        State e4 = new State("E4");
        State e5 = new State("E5");
        State e6 = new State("E6");
        State e7 = new State("E7");
        State e8 = new State("E8");
        State e9 = new State("E9");
        State e10 = new State("E10");
        State e11 = new State("E11");
        State e12 = new State("E12");
        State e13 = new State("E13");

        // Transitions from e0
        for (String str: letters) {
            e0.addTransition(str, e1);
        }

        // Transitions from e1
        for (String str: letters) {
            e1.addTransition(str, e1);
        }
        for (String str: numbers) {
            e1.addTransition(str, e1);
        }
        e1.addTransition("-", e2);
        e1.addTransition(".", e2);
        e1.addTransition("@", e8);

        // Transitions from e2
        for (String str: letters) {
            e2.addTransition(str, e3);
        }

        // Transitions from e3
        for (String str: letters) {
            e3.addTransition(str, e3);
        }
        for (String str: numbers) {
            e3.addTransition(str, e3);
        }
        e3.addTransition(".", e4);
        e3.addTransition("@", e8);

        // Transitions from e4
        for (String str: letters) {
            e4.addTransition(str, e5);
        }

        // Transitions from e5
        for (String str: letters) {
            e5.addTransition(str, e5);
        }
        for (String str: numbers) {
            e5.addTransition(str, e5);
        }
        e5.addTransition("-", e6);
        e5.addTransition("@", e8);

        // Transitions from e6
        for (String str: letters) {
            e6.addTransition(str, e7);
        }

        // Transitions from e7
        for (String str: letters) {
            e7.addTransition(str, e7);
        }
        for (String str: numbers) {
            e7.addTransition(str, e7);
        }
        e7.addTransition("@", e8);

        // Transitions from e8
        for (String str: letters) {
            e8.addTransition(str, e9);
        }

        // Transitions from e9
        for (String str: letters) {
            e9.addTransition(str, e9);
        }
        for (String str: numbers) {
            e9.addTransition(str, e9);
        }
        e9.addTransition("-", e10);
        e9.addTransition(".", e12);

        // Transitions from e10
        for (String str: letters) {
            e10.addTransition(str, e11);
        }

        // Transitions from e11
        for (String str: letters) {
            e11.addTransition(str, e11);
        }
        for (String str: numbers) {
            e11.addTransition(str, e11);
        }
        e11.addTransition(".", e12);

        // Transitions from e12
        for (String str: letters) {
            e12.addTransition(str, e13);
        }

        // Transitions from e13
        for (String str: letters) {
            e13.addTransition(str, e13);
        }
        e13.addTransition(";", e0);


        states.add(e0);
        states.add(e1);
        states.add(e2);
        states.add(e3);
        states.add(e4);
        states.add(e5);
        states.add(e6);
        states.add(e7);
        states.add(e8);
        states.add(e9);
        states.add(e10);
        states.add(e11);
        states.add(e12);
        states.add(e13);
        a.setStates(states);

        // Set initial state
        a.setInitialState(e0);

        // Set finals States
        List<State> fStates = new LinkedList<>();
        fStates.add(e13);
        a.setFinalsStates(fStates);

        return a;
    }

    /**
     * Method to test an automata of emails addresses with given strings.
     * Use the method EmailCharger().
     */
    static void EmailTester() {
        Automata a = EmailCharger();
        String strings[] = {"a@b.c", "eliott.dubois@gmail.com;DBS.eliott@free.fr", "Satan666-hell@hotlook.notHeaven"};

        for (String str: strings) {
            Application.execOne(str, a);
        }
    }
}
