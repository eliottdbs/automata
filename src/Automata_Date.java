/**
 * @author DUBOIS Eliott
 * @version 1.0
 * Created  on : 2019-12-05
 * Modified on : 2019-12-05
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains all the necessary to check a date.
 */
public class Automata_Date {

    /**
     * Build the automata to test a string as a date.
     * The accepted format is DD/MM/YYYY.
     * @return the built automata
     */
    static Automata DateCharger() {

        Automata a = new Automata();

        // Set Alphabet
        List<String> zero = new LinkedList<>(Arrays.asList("0"));
        List<String> numbers = new LinkedList<>(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9"));
        List<String> specialCaracters = new LinkedList<>(Arrays.asList("/"));

        List<String> alphabet = new LinkedList<>();
        alphabet.addAll(numbers);
        alphabet.addAll(specialCaracters);
        a.setAlphabet(alphabet);

        // Set States
        List<State> states = new LinkedList<>();
        State e0 = new State("E0");
        State j1 = new State("J1");
        State j2 = new State("J2");
        State j3 = new State("J3");
        State j4 = new State("J4");
        State j5 = new State("J5");
        State j6 = new State("J6");
        State s1 = new State("S1");
        State s2 = new State("S2");
        State s3 = new State("S3");
        State s4 = new State("S4");
        State m1 = new State("M1");
        State m2 = new State("M2");
        State m3 = new State("M3");
        State m4 = new State("M4");
        State m5 = new State("M5");
        State m6 = new State("M6");
        State m7 = new State("M7");
        State m8 = new State("M8");
        State m9 = new State("M9");
        State a1 = new State("A1");
        State a2 = new State("A2");
        State a3 = new State("A3");
        State a4 = new State("A4");
        State a5 = new State("A5");
        State a6 = new State("A6");
        State a7 = new State("A7");

        e0.addTransition("0", j1);
        e0.addTransition("1", j2);
        e0.addTransition("2", j2);
        e0.addTransition("3", j3);

        for (String str: numbers) {
            j1.addTransition(str, j4);
        }

        j2.addTransition("0", j4);
        for (String str: numbers) {
            j2.addTransition(str, j4);
        }

        j3.addTransition("0", j5);
        j3.addTransition("1", j6);

        j4.addTransition("/", s1);

        j5.addTransition("/", s2);

        j6.addTransition("/", s3);

        s1.addTransition("0", m1);
        s1.addTransition("1", m2);

        s2.addTransition("0", m3);
        s2.addTransition("1", m4);

        s3.addTransition("0", m5);
        s3.addTransition("1", m6);

        for (String str: numbers) {
            m1.addTransition(str, m7);
        }

        m2.addTransition("0", m7);
        m2.addTransition("1", m7);
        m2.addTransition("2", m7);

        m3.addTransition("1", m8);
        m3.addTransition("3", m8);
        m3.addTransition("4", m8);
        m3.addTransition("5", m8);
        m3.addTransition("6", m8);
        m3.addTransition("7", m8);
        m3.addTransition("8", m8);
        m3.addTransition("9", m8);

        m4.addTransition("0", m8);
        m4.addTransition("1", m8);
        m4.addTransition("2", m8);

        m5.addTransition("1", m9);
        m5.addTransition("3", m9);
        m5.addTransition("5", m9);
        m5.addTransition("7", m9);
        m5.addTransition("8", m9);

        m6.addTransition("0", m9);
        m6.addTransition("2", m9);

        m7.addTransition("/", s4);

        m8.addTransition("/", s4);

        m9.addTransition("/", s4);

        s4.addTransition("0", a1);
        for (String str: numbers) {
            s4.addTransition(str, a2);
        }

        a1.addTransition("0", a3);
        for (String str: numbers) {
            a1.addTransition(str, a4);
        }

        a2.addTransition("0", a4);
        for (String str: numbers) {
            a2.addTransition(str, a4);
        }

        a3.addTransition("0", a5);
        for (String str: numbers) {
            a3.addTransition(str, a6);
        }

        a4.addTransition("0", a6);
        for (String str: numbers) {
            a4.addTransition(str, a6);
        }

        for (String str: numbers) {
            a5.addTransition(str, a7);
        }

        a6.addTransition("0", a7);
        for (String str: numbers) {
            a6.addTransition(str, a7);
        }


        states.add(e0);
        states.add(j1);
        states.add(j2);
        states.add(j3);
        states.add(j4);
        states.add(j5);
        states.add(j6);
        states.add(s1);
        states.add(s2);
        states.add(s3);
        states.add(s4);
        states.add(m1);
        states.add(m2);
        states.add(m3);
        states.add(m4);
        states.add(m5);
        states.add(m6);
        states.add(m7);
        states.add(m8);
        states.add(m9);
        states.add(a1);
        states.add(a2);
        states.add(a3);
        states.add(a4);
        states.add(a5);
        states.add(a6);
        states.add(a7);
        a.setStates(states);

        // Set initial state
        a.setInitialState(e0);

        // Set finals States
        List<State> fStates = new LinkedList<>();
        fStates.add(a7);
        a.setFinalsStates(fStates);

        return a;
    }

    /**
     * Method to test an automata of dates with given strings.
     * Use the method DateCharger().
     */
    static void DateTester() {
        Automata a = DateCharger();
        String strings[] = {"01/01/0001", "11/11/2001", "21/01/0511", "30/01/0001", "29/02/0845", "31/03/5464", "31/04/0001", "365/01/0001", "01/011/0001", "01/01/000"};

        for (String str : strings) {
            Application.execOne(str, a);
        }
    }
}
