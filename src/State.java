/**
 * @author DUBOIS Eliott
 * @version 1.0
 * Created  on : 2019-11-12
 * Modified on : 2019-12-05
 */

import java.util.HashMap;

/**
 * This class contains the attributes and methods that represent a state.
 * States are used in automata to pass to another state with a specific label.
 */
public class State {

    private String name;
    private HashMap<String, State> transitions;

    /**
     * Initialize a state.
     * @param stateName the state's name
     */
    public State(String stateName) {
        this.name = stateName;
        this.transitions = new HashMap<>();
    }

    /**
     * Getter of the state's name.
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter of the state's name.
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to add a transition to the state's transitions list.
     * Transitions allow an automata to pass from a state to another via a label.
     * @param label the label link to the next state
     * @param state the next state
     */
    public void addTransition(String label, State state) {
        if (!this.transitions.containsKey(label)) {
            this.transitions.put(label, state);
        }
    }

    /**
     * Getter of the state's transitions list.
     * @return the transitions list
     */
    public HashMap<String, State> getTransitions() {
        return this.transitions;
    }

    /**
     * Getter of the next state with a given label.
     * @param label the string associated with the presumed next state
     * @return a state already initialize if exists or a null state if not
     */
    public State nextState(String label) {
        return this.transitions.get(label);
    }


    /**
     * Method to remove a transition of the transitions list with a given label.
     * @param label the string associated with the presumed state to remove
     */
    public void removeTransition(String label) {
        if (this.transitions.containsKey(label)) {
            this.transitions.remove(label);
        }
    }

    /**
     * Method to get the state description as a string.
     * @return a string containing the state's name
     */
    public String toString() {
        return this.name;
    }
}
