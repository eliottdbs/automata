# Automata

This project is a Java library used to build [finite-state machines](https://en.wikipedia.org/wiki/Finite-state_machine) to test the validy of string.

## Installation

The minimum to import are the `Automata` and `State` classes in the `src` file.

## Example

### Create a specific automata

Before coding, you need to know the alphabet and states that will compose your automata. It can be resumed to a simple table. For example, an automata that recognizes only few smileys ( *:-) :-( ;-) ;-( :=) :=(* ) will have this state transition table :
<h5>State transition table of a smiley automata</h5>
<table>
<tr>
<th>State / Alphabet</th>
<th>:</th>
<th>;</th>
<th>-</th>
<th>=</th>
<th>)</th>
<th>(</th>
</tr>
<tr>
<th>State0</th>
<td>Eye1</td>
<td>Eye2</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<th>Eye1</th>
<td></td>
<td></td>
<td>Nose</td>
<td>Nose</td>
<td></td>
<td></td>
</tr>
<tr>
<th>Eye2</th>
<td></td>
<td></td>
<td>Nose</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<th>Nose</th>
<td></td>
<td></td>
<td></td>
<td></td>
<td>Mouth</td>
<td>Mouth</td>
</tr>
<th>Mouth</th>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>

The code associated to this state transition table is :

```java
static Automata SmileyCharger() {

    Automata automata = new Automata();


    // Set Alphabet
    List<String> alphabet = new LinkedList<>(Arrays.asList(":", ";", "-", "=", ")", "("));
    automata.setAlphabet(alphabet);


    // Set States
    List<State> states = new LinkedList<>();

    State state0 = new State("State0");
    State eye1 = new State("Eye1");
    State eye2 = new State("Eye2");
    State nose = new State("Nose");
    State mouth = new State("Mouth");

    state0.addTransition(":", eye1);
    state0.addTransition(";", eye2);

    eye1.addTransition("-", nose);
    eye1.addTransition("=", nose);

    eye2.addTransition("-", nose);

    nose.addTransition("(", mouth);
    nose.addTransition(")", mouth);

    states.add(state0);
    states.add(eye1);
    states.add(eye2);
    states.add(nose);
    states.add(mouth);

    automata.setStates(states);


    // Set initial state
    automata.setInitialState(state0);


    // Set finals States
    List<State> fStates = new LinkedList<>();
    fStates.add(mouth);
    automata.setFinalsStates(fStates);

    return automata;
}
```

### Use an automata

Here is an example of the `main` function :

```java
public static void main(String[] args) {

    // Input init
    Scanner scan = new Scanner(System.in);
    String choice = "1";

    // Main loop
    while (!choice.equals("0")) {

        // Input choice
        choice = scan.nextLine();

        switch (choice) {

            case "1":
                String input_smiley = scan.nextLine();

                Automata smiley = Automata_Smiley.SmileyCharger();
                smiley.nObjectTester(input_smiley)

                break;


            default:
                choice = "0";
                break;

        }

    }

}
```

## Documentation

All the javadoc has been generated in the `doc` file.

## License

This project is licensed under MIT License - see the [LICENSE](https://gitlab.com/eliott.dubois/finite-state-machine/blob/master/LICENSE) file for details.