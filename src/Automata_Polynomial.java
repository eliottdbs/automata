/**
 * @author DUBOIS Eliott
 * @version 1.0
 * Created  on : 2019-12-06
 * Modified on : 2019-12-06
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains all the necessary to check a polynomial expression.
 */

public class Automata_Polynomial {

    /**
     * Build the automata to test a string as a polynomial expression.
     * This automata accept only the following polynomial expression :
     *      09X**2 ± 09X ± 09
     *      with numbers between 1 and 25
     * @return the built automata
     */
    static Automata PolynomialCharger() {

        Automata a = new Automata();

        // Set Alphabet
        List<String> numbers = new LinkedList<>(Arrays.asList("3", "4", "5", "6", "7", "8", "9"));
        List<String> specialCaracters = new LinkedList<>(Arrays.asList("+", "-", " ", "X", "*"));

        List<String> alphabet = new LinkedList<>();
        alphabet.add("0");
        alphabet.add("1");
        alphabet.add("2");
        alphabet.addAll(numbers);
        alphabet.addAll(specialCaracters);
        a.setAlphabet(alphabet);

        // Set States
        List<State> states = new LinkedList<>();
        State e0 = new State("E0");
        State e1 = new State("E1");
        State e2 = new State("E2");
        State e3 = new State("E3");
        State e4 = new State("E4");
        State e5 = new State("E5");
        State e6 = new State("E6");
        State e7 = new State("E7");
        State e8 = new State("E8");
        State e9 = new State("E9");
        State e10 = new State("E10");
        State e11 = new State("E11");
        State e12 = new State("E12");
        State e13 = new State("E13");
        State e14 = new State("E14");
        State e15 = new State("E15");
        State e16 = new State("E16");

        // Transitions from e0
        for (String str: numbers) {
            e0.addTransition(str, e2);
        }
        e0.addTransition("1", e1);
        e0.addTransition("2", e3);

        // Transitions from e1
        e1.addTransition("0", e2);
        e1.addTransition("1", e2);
        e1.addTransition("2", e2);
        for (String str: numbers) {
            e1.addTransition(str, e2);
        }
        e1.addTransition("X", e4);

        // Transitions from e2
        e2.addTransition("X", e4);

        // Transitions from e3
        e3.addTransition("0", e2);
        e3.addTransition("1", e2);
        e3.addTransition("2", e2);
        e3.addTransition("3", e2);
        e3.addTransition("4", e2);
        e3.addTransition("5", e2);
        e3.addTransition("X", e4);

        // Transitions from e4
        e4.addTransition("*", e5);

        // Transitions from e5
        e5.addTransition("*", e6);

        // Transitions from e6
        e6.addTransition("2", e7);

        // Transitions from e7
        e7.addTransition(" ", e7);
        e7.addTransition("+", e8);
        e7.addTransition("-", e8);

        // Transitions from e8
        e8.addTransition(" ", e8);
        e8.addTransition("1", e9);
        e8.addTransition("2", e10);
        for (String str: numbers) {
            e8.addTransition(str, e11);
        }

        // Transitions from e9
        e9.addTransition("0", e11);
        e9.addTransition("1", e11);
        e9.addTransition("2", e11);
        for (String str: numbers) {
            e9.addTransition(str, e11);
        }
        e9.addTransition("X", e12);

        // Transitions from e10
        e10.addTransition("0", e11);
        e10.addTransition("1", e11);
        e10.addTransition("2", e11);
        e10.addTransition("3", e11);
        e10.addTransition("4", e11);
        e10.addTransition("5", e11);
        e10.addTransition("X", e12);

        // Transitions from e11
        e11.addTransition("X", e12);

        // Transitions from e12
        e12.addTransition(" ", e12);
        e12.addTransition("+", e13);
        e12.addTransition("-", e13);

        // Transitions from e13
        e13.addTransition(" ", e13);
        e13.addTransition("1", e14);
        e13.addTransition("2", e15);
        for (String str: numbers) {
            e13.addTransition(str, e16);
        }

        // Transitions from e14
        e14.addTransition(" ", e16);
        e14.addTransition("0", e16);
        e14.addTransition("1", e16);
        e14.addTransition("2", e16);
        for (String str: numbers) {
            e14.addTransition(str, e16);
        }

        // Transitions from e15
        e15.addTransition(" ", e16);
        e15.addTransition("0", e16);
        e15.addTransition("1", e16);
        e15.addTransition("2", e16);
        e15.addTransition("3", e16);
        e15.addTransition("4", e16);
        e15.addTransition("5", e16);

        // Transitions from e16
        e16.addTransition(" ", e16);

        states.add(e0);
        states.add(e1);
        states.add(e2);
        states.add(e3);
        states.add(e4);
        states.add(e5);
        states.add(e6);
        states.add(e7);
        states.add(e8);
        states.add(e9);
        states.add(e10);
        states.add(e11);
        states.add(e12);
        states.add(e13);
        states.add(e14);
        states.add(e15);
        states.add(e16);
        a.setStates(states);

        // Set initial state
        a.setInitialState(e0);

        // Set finals States
        List<State> fStates = new LinkedList<>(Arrays.asList(e14, e15, e16));
        fStates.addAll(fStates);
        a.setFinalsStates(fStates);

        return a;
    }

    /**
     * Method to test an automata of polynomial expressions with given strings.
     * Use the method PolynomialCharger().
     */
    static void PolynomialTester() {
        Automata a = PolynomialCharger();
        String strings[] = {"8X**2 - 15X + 3", "8X**2-15X+3", "8X**2   -  15X +   3", "25X**2 - 1X + 25", "26X**2 - 22X + 16"};

        for (String str: strings) {
            Application.execOne(str, a);
        }
    }
}
